package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysCloumn;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
public interface SysCloumnMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    public SysCloumn selectSysCloumnById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysCloumn 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SysCloumn> selectSysCloumnList(SysCloumn sysCloumn);

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysCloumn 【请填写功能名称】
     * @return 结果
     */
    public int insertSysCloumn(SysCloumn sysCloumn);

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysCloumn 【请填写功能名称】
     * @return 结果
     */
    public int updateSysCloumn(SysCloumn sysCloumn);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysCloumnById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysCloumnByIds(String[] ids);
}
