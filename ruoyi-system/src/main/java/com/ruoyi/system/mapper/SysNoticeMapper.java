package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysNotice;

import java.util.List;

/**
 * 内容Mapper接口
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
public interface SysNoticeMapper 
{
    /**
     * 查询内容
     * 
     * @param noticeId 内容ID
     * @return 内容
     */
    public SysNotice selectSysNoticeById(Long noticeId);

    /**
     * 查询内容列表
     * 
     * @param sysNotice 内容
     * @return 内容集合
     */
    public List<SysNotice> selectSysNoticeList(SysNotice sysNotice);

    /**
     * 新增内容
     * 
     * @param sysNotice 内容
     * @return 结果
     */
    public int insertSysNotice(SysNotice sysNotice);

    /**
     * 修改内容
     * 
     * @param sysNotice 内容
     * @return 结果
     */
    public int updateSysNotice(SysNotice sysNotice);

    /**
     * 删除内容
     * 
     * @param noticeId 内容ID
     * @return 结果
     */
    public int deleteSysNoticeById(Integer noticeId);

    /**
     * 批量删除内容
     * 
     * @param noticeIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysNoticeByIds(String[] noticeIds);

    public List<SysNotice> selHist();


    public List<SysNotice> selTitle(SysNotice sysNotice);
}
