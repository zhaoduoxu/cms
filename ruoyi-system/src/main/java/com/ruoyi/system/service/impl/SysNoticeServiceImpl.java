package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.service.ISysNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 内容Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
@Service
public class SysNoticeServiceImpl implements ISysNoticeService
{
    @Autowired
    private SysNoticeMapper sysNoticeMapper;

    /**
     * 查询内容
     * 
     * @param noticeId 内容ID
     * @return 内容
     */
    @Override
    public SysNotice selectSysNoticeById(Long noticeId)
    {
        SysNotice sysNotice = sysNoticeMapper.selectSysNoticeById(noticeId);
        if (sysNotice.getHits()==null||sysNotice.getHits()==0){
            Long hist = new Long(1);
            sysNotice.setHits(hist);
            sysNotice.setNoticeId(noticeId);
            sysNoticeMapper.updateSysNotice(sysNotice);
        }else {
            sysNotice.setHits(sysNotice.getHits() + 1);
            sysNotice.setNoticeId(noticeId);
            sysNoticeMapper.updateSysNotice(sysNotice);
        }
        return sysNotice;
    }

    /**
     * 查询内容列表
     * 
     * @param sysNotice 内容
     * @return 内容
     */
    @Override
    public List<SysNotice> selectSysNoticeList(SysNotice sysNotice)
    {
        return sysNoticeMapper.selTitle(sysNotice);
    }

    @Override
    public List<SysNotice> selectSysNoticeLists(SysNotice sysNotice)
    {
        return sysNoticeMapper.selectSysNoticeList(sysNotice);
    }

    /**
     * 新增内容
     * 
     * @param sysNotice 内容
     * @return 结果
     */
    @Override
    public int insertSysNotice(SysNotice sysNotice)
    {
        if (sysNotice.getCreateTime()==null){
            sysNotice.setCreateTime(DateUtils.getNowDate());
        }
        if (sysNotice.getIsShuffling()==0){
            sysNotice.setShufflingType(0L);
        }else {
            sysNotice.setShufflingType(sysNotice.getShufflingType());
        }
        return sysNoticeMapper.insertSysNotice(sysNotice);
    }

    /**
     * 修改内容
     * 
     * @param sysNotice 内容
     * @return 结果
     */
    @Override
    public int updateSysNotice(SysNotice sysNotice)
    {
        sysNotice.setUpdateTime(DateUtils.getNowDate());
        if (null == sysNotice.getIsShuffling()||sysNotice.getIsShuffling()==0){
            sysNotice.setShufflingType(0L);
        }else {
            sysNotice.setShufflingType(sysNotice.getShufflingType());
        }
        return sysNoticeMapper.updateSysNotice(sysNotice);
    }

    /**
     * 删除内容对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysNoticeByIds(String ids)
    {
        return sysNoticeMapper.deleteSysNoticeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除内容信息
     * 
     * @param noticeId 内容ID
     * @return 结果
     */
    public int deleteSysNoticeById(Integer noticeId)
    {
        return sysNoticeMapper.deleteSysNoticeById(noticeId);
    }

    @Override
    public List<SysNotice> selHist() {

        return sysNoticeMapper.selHist();
    }


}
