package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysNotice;

import java.util.List;

/**
 * 内容Service接口
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
public interface ISysNoticeService 
{
    /**
     * 查询内容
     * 
     * @param noticeId 内容ID
     * @return 内容
     */
    public SysNotice selectSysNoticeById(Long noticeId);

    /**
     * 查询内容列表
     * 
     * @param sysNotice 内容
     * @return 内容集合
     */
    public List<SysNotice> selectSysNoticeList(SysNotice sysNotice);




    public List<SysNotice> selectSysNoticeLists(SysNotice sysNotice);

    /**
     * 新增内容
     * 
     * @param sysNotice 内容
     * @return 结果
     */
    public int insertSysNotice(SysNotice sysNotice);

    /**
     * 修改内容
     * 
     * @param sysNotice 内容
     * @return 结果
     */
    public int updateSysNotice(SysNotice sysNotice);

    /**
     * 批量删除内容
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysNoticeByIds(String ids);

    /**
     * 删除内容信息
     * 
     * @param noticeId 内容ID
     * @return 结果
     */
    public int deleteSysNoticeById(Integer noticeId);


    public List<SysNotice> selHist();


}
