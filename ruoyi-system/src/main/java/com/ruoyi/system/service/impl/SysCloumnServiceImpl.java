package com.ruoyi.system.service.impl;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysCloumn;
import com.ruoyi.system.mapper.SysCloumnMapper;
import com.ruoyi.system.mapper.SysNoticeMapper;
import com.ruoyi.system.service.ISysCloumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
@Service
public class SysCloumnServiceImpl extends BaseController implements ISysCloumnService
{
    @Autowired
    private SysCloumnMapper sysCloumnMapper;
    @Autowired
    private SysNoticeMapper noticeMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】ID
     * @return 【请填写功能名称】
     */
    @Override
    public SysCloumn selectSysCloumnById(Long id)
    {
        return sysCloumnMapper.selectSysCloumnById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysCloumn 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<SysCloumn> selectSysCloumnList(SysCloumn sysCloumn)
    {
        return sysCloumnMapper.selectSysCloumnList(sysCloumn);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysCloumn 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertSysCloumn(SysCloumn sysCloumn)
    {
        sysCloumn.setCreateTime(DateUtils.getNowDate());
        return sysCloumnMapper.insertSysCloumn(sysCloumn);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param sysCloumn 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateSysCloumn(SysCloumn sysCloumn)
    {
        sysCloumn.setUpdateTime(DateUtils.getNowDate());
        return sysCloumnMapper.updateSysCloumn(sysCloumn);
    }

    /**
     * 删除【请填写功能名称】对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSysCloumnByIds(String ids)
    {
        return sysCloumnMapper.deleteSysCloumnByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】ID
     * @return 结果
     */
    public int deleteSysCloumnById(Long id)
    {
        return sysCloumnMapper.deleteSysCloumnById(id);
    }

    @Override
    public List<Ztree> selectTree() {
        List<SysCloumn> list = sysCloumnMapper.selectSysCloumnList(new SysCloumn());

        List<Ztree> ztrees = initZtree(list);
        return ztrees;
    }

    @Override
    public List<Ztree> selectCloumnTree() {
        SysCloumn sysCloumn = new SysCloumn();
        sysCloumn.setPid(0L);
        List<SysCloumn> list = sysCloumnMapper.selectSysCloumnList(sysCloumn);
        List<Ztree> ztrees = initCloumnZtree(list);
        return ztrees;
    }

    public List<Ztree> initZtree(List<SysCloumn> list) {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (SysCloumn cloumn : list)
        {
                Ztree ztree = new Ztree();
                ztree.setId(cloumn.getId());
                ztree.setpId(cloumn.getPid());
                ztree.setName(cloumn.getName());
                ztree.setTitle(cloumn.getName());

                ztrees.add(ztree);

        }
        return ztrees;
    }

    public List<Ztree> initCloumnZtree(List<SysCloumn> list) {

        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (SysCloumn cloumn : list)
        {
            Ztree ztree = new Ztree();
            ztree.setId(cloumn.getId());
         /*   ztree.setpId(cloumn.getPid());*/
            ztree.setName(cloumn.getName());
            ztree.setTitle(cloumn.getName());

            ztrees.add(ztree);

        }
        return ztrees;
    }


}
