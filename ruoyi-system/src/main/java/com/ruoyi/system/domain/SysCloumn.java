package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 sys_cloumn
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
public class SysCloumn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 栏目名称 */
    @Excel(name = "栏目名称")
    private String name;

    /** 栏目code  备用 */
    @Excel(name = "栏目code  备用")
    private String code;

    /** 栏目状态  是否启用 */
    @Excel(name = "栏目状态  是否启用")
    private Integer status;

    /** $column.columnComment */
    @Excel(name = "栏目状态  是否启用")
    private String pname;

    /** $column.columnComment */
    @Excel(name = "栏目状态  是否启用")
    private Long pid;

    /** 跳转url */
    @Excel(name = "跳转url")
    private String url;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Integer sortpath;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setPname(String pname)
    {
        this.pname = pname;
    }

    public String getPname() 
    {
        return pname;
    }
    public void setPid(Long pid) 
    {
        this.pid = pid;
    }

    public Long getPid() 
    {
        return pid;
    }
    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }

    public Integer getSortpath() {
        return sortpath;
    }

    public void setSortpath(Integer sortpath) {
        this.sortpath = sortpath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("code", getCode())
            .append("status", getStatus())
            .append("pname", getPname())
            .append("pid", getPid())
            .append("createTime", getCreateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("url", getUrl())
            .append("sortpath", getSortpath())
            .toString();
    }
}
