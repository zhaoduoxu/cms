package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 内容对象 sys_notice
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
public class SysNotice extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文章ID */
    private Long noticeId;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String noticeTitle;

    /** 文章类型（1通知 2公告） */
    @Excel(name = "文章类型", readConverterExp = "1=通知,2=公告")
    private String noticeType;

    /** 文章内容 */
    @Excel(name = "文章内容")
    private String noticeContent;

    /** 文章状态（0正常 1关闭） */
    @Excel(name = "文章状态", readConverterExp = "0=正常,1=关闭")
    private String status;

    /** 栏目id */
    @Excel(name = "栏目id")
    private Long cloumnId;

    /** 栏目名称 */
    @Excel(name = "栏目名称")
    private String cloumnName;

    /** $column.columnComment */
    @Excel(name = "栏目名称")
    private String imageurl;

    /** $column.columnComment */
    @Excel(name = "栏目名称")
    private String sortpath;

    /** $column.columnComment */
    @Excel(name = "栏目名称")
    private String comefrom;

    /** $column.columnComment */
    @Excel(name = "栏目名称")
    private Long hits;

    /** $column.columnComment */
    @Excel(name = "栏目名称")
    private String author;

    /** 轮播分类  1 首页轮播  2 尾部轮播 */
    @Excel(name = "轮播分类  1 首页轮播  2 尾部轮播")
    private Long shufflingType;

    /** 是否轮播  0否 1是 */
    @Excel(name = "是否轮播  0否 1是")
    private Long isShuffling;

    /** 第二标题 */
    @Excel(name = "第二标题")
    private String subtitle;

    /** 1头条  0默认 */
    @Excel(name = "1头条  0默认")
    private Long hot;

    private SysCloumn cloumn;

    public Long getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Long noticeId) {
        this.noticeId = noticeId;
    }

    public void setNoticeTitle(String noticeTitle)
    {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeTitle() 
    {
        return noticeTitle;
    }
    public void setNoticeType(String noticeType) 
    {
        this.noticeType = noticeType;
    }

    public String getNoticeType() 
    {
        return noticeType;
    }
    public void setNoticeContent(String noticeContent) 
    {
        this.noticeContent = noticeContent;
    }

    public String getNoticeContent() 
    {
        return noticeContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setCloumnId(Long cloumnId) 
    {
        this.cloumnId = cloumnId;
    }

    public Long getCloumnId() 
    {
        return cloumnId;
    }
    public void setCloumnName(String cloumnName) 
    {
        this.cloumnName = cloumnName;
    }

    public String getCloumnName() 
    {
        return cloumnName;
    }
    public void setImageurl(String imageurl) 
    {
        this.imageurl = imageurl;
    }

    public String getImageurl() 
    {
        return imageurl;
    }
    public void setSortpath(String sortpath) 
    {
        this.sortpath = sortpath;
    }

    public String getSortpath() 
    {
        return sortpath;
    }
    public void setComefrom(String comefrom) 
    {
        this.comefrom = comefrom;
    }

    public String getComefrom() 
    {
        return comefrom;
    }
    public void setHits(Long hits) 
    {
        this.hits = hits;
    }

    public Long getHits() 
    {
        return hits;
    }
    public void setAuthor(String author) 
    {
        this.author = author;
    }

    public String getAuthor() 
    {
        return author;
    }
    public void setShufflingType(Long shufflingType) 
    {
        this.shufflingType = shufflingType;
    }

    public Long getShufflingType() 
    {
        return shufflingType;
    }
    public void setIsShuffling(Long isShuffling) 
    {
        this.isShuffling = isShuffling;
    }

    public Long getIsShuffling() 
    {
        return isShuffling;
    }
    public void setSubtitle(String subtitle) 
    {
        this.subtitle = subtitle;
    }

    public String getSubtitle() 
    {
        return subtitle;
    }
    public void setHot(Long hot)
    {
        this.hot = hot;
    }

    public Long getHot()
    {
        return hot;
    }
    public SysCloumn getCloumn() {
        if (cloumn==null){
            cloumn =new SysCloumn();
        }
        return cloumn;
    }

    public void setCloumn(SysCloumn cloumn) {
        this.cloumn = cloumn;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("noticeId", getNoticeId())
            .append("noticeTitle", getNoticeTitle())
            .append("noticeType", getNoticeType())
            .append("noticeContent", getNoticeContent())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("cloumnId", getCloumnId())
            .append("cloumnName", getCloumnName())
            .append("imageurl", getImageurl())
            .append("sortpath", getSortpath())
            .append("comefrom", getComefrom())
            .append("hits", getHits())
            .append("author", getAuthor())
            .append("shufflingType", getShufflingType())
            .append("isShuffling", getIsShuffling())
            .append("subtitle", getSubtitle())
                .append("hot", getHot())
                .append("cloumn", getCloumn())
            .toString();
    }
}
