package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.util.ShiroUtils;
import com.ruoyi.system.domain.SysCloumn;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.service.ISysCloumnService;
import com.ruoyi.system.service.ISysNoticeService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 内容Controller
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
@Controller
@RequestMapping("/system/notice")
@Api(tags = "文章管理")
public class SysNoticeController extends BaseController
{
    private String prefix = "system/notice";

    @Autowired
    private ISysNoticeService sysNoticeService;
    @Autowired
   private ISysCloumnService sysCloumnService;

    @RequiresPermissions("system:notice:view")
    @GetMapping()
    public String notice()
    {
        return prefix + "/notice";
    }

    /**
     * 查询内容列表
     */
    @RequiresPermissions("system:notice:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysNotice sysNotice)
    {
        startPage();
        List<SysNotice> list = sysNoticeService.selectSysNoticeLists(sysNotice);
        return getDataTable(list);
    }

    /**
     * 导出内容列表
     */
    @RequiresPermissions("system:notice:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysNotice sysNotice)
    {
        List<SysNotice> list = sysNoticeService.selectSysNoticeList(sysNotice);
        ExcelUtil<SysNotice> util = new ExcelUtil<SysNotice>(SysNotice.class);
        return util.exportExcel(list, "notice");
    }

   /* *//**
     * 新增内容
     *//*
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") Long parentId, ModelMap mmap)
    {
        mmap.put("cloumn", sysCloumnService.selectSysCloumnById(parentId));
        return prefix + "/add";
    }*/
    /**
     * 新增内容
     */
    @GetMapping("/add")
    public String add( ModelMap mmap)
    {
        SysCloumn sysCloumn = new SysCloumn();
        sysCloumn.setPid(new Long(0));
        List<SysCloumn> cloumn = sysCloumnService.selectSysCloumnList(sysCloumn);
        mmap.put("cloumns",cloumn);
        return prefix + "/add";
    }

    /**
     * 新增保存内容
     */
    @RequiresPermissions("system:notice:add")
    @Log(title = "内容", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysNotice sysNotice)
    {
        sysNotice.setCreateBy( ShiroUtils.getLoginName());
        return toAjax(sysNoticeService.insertSysNotice(sysNotice));
    }

    /**
     * 修改内容
     */
    @GetMapping("/edit/{noticeId}")
    public String edit(@PathVariable("noticeId") Long noticeId, ModelMap mmap)
    {
        SysNotice sysNotice = sysNoticeService.selectSysNoticeById(noticeId);
        SysCloumn sysCloumn = sysCloumnService.selectSysCloumnById(sysNotice.getCloumnId());
        mmap.put("sysCloumn",sysCloumn);
        mmap.put("sysNotice", sysNotice);

        return prefix + "/edit";
    }

    /**
     * 修改保存内容
     */
    @RequiresPermissions("system:notice:edit")
    @Log(title = "内容", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysNotice sysNotice)
    {
        sysNotice.setUpdateBy( ShiroUtils.getLoginName());
        return toAjax(sysNoticeService.updateSysNotice(sysNotice));
    }

    /**
     * 删除内容
     */
    @RequiresPermissions("system:notice:remove")
    @Log(title = "内容", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysNoticeService.deleteSysNoticeByIds(ids));
    }


    /**
     * 修改公告
     */
    @GetMapping("/preview/{noticeId}")
    public String preview(@PathVariable("noticeId") Long noticeId, ModelMap mmap)
    {
        mmap.put("notice", sysNoticeService.selectSysNoticeById(noticeId));
        return prefix + "/preview";
    }


    /**
     * 修改头条状态
     */
    @RequiresPermissions("system:notice:edit")
    @Log(title = "修改栏目状态", businessType = BusinessType.UPDATE)
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(SysNotice sysNotice)
    {

        return toAjax(sysNoticeService.updateSysNotice(sysNotice));
    }
}
