package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysCloumn;
import com.ruoyi.system.service.ISysCloumnService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2019-11-09
 */
@Controller
@RequestMapping("/system/cloumn")
@Api(tags = "栏目管理")
public class SysCloumnController extends BaseController
{
    private String prefix = "system/cloumn";

    @Autowired
    private ISysCloumnService sysCloumnService;

    @RequiresPermissions("system:cloumn:view")
    @GetMapping()
    public String cloumn()
    {
        return prefix + "/cloumn";
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @RequiresPermissions("system:cloumn:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SysCloumn sysCloumn)
    {
        startPage();
        List<SysCloumn> list = sysCloumnService.selectSysCloumnList(sysCloumn);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @RequiresPermissions("system:cloumn:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SysCloumn sysCloumn)
    {
        List<SysCloumn> list = sysCloumnService.selectSysCloumnList(sysCloumn);
        ExcelUtil<SysCloumn> util = new ExcelUtil<SysCloumn>(SysCloumn.class);
        return util.exportExcel(list, "cloumn");
    }

    /**
     * 新增【请填写功能名称】
     */
    @GetMapping("/add")

    public String add(ModelMap mmap)
    {
        SysCloumn sysCloumn = new SysCloumn();
        sysCloumn.setPid(new Long(0));
        List<SysCloumn> cloumn = sysCloumnService.selectSysCloumnList(sysCloumn);
        mmap.put("cloumns",cloumn);
        return prefix + "/add";
    }

    /**
     * 新增保存【请填写功能名称】
     */
    @RequiresPermissions("system:cloumn:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SysCloumn sysCloumn
    )
    {
        return toAjax(sysCloumnService.insertSysCloumn(sysCloumn));
    }

    /**
     * 修改【请填写功能名称】
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysCloumn sysCloumns = new SysCloumn();
        sysCloumns.setPid(new Long(0));
        List<SysCloumn> cloumn = sysCloumnService.selectSysCloumnList(sysCloumns);
        SysCloumn sysCloumn = sysCloumnService.selectSysCloumnById(id);
        mmap.put("sysCloumn", sysCloumn);
        mmap.put("cloumns",cloumn);
        return prefix + "/edit";
    }

    /**
     * 修改保存【请填写功能名称】
     */
    @RequiresPermissions("system:cloumn:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SysCloumn sysCloumn)
    {
        SysCloumn cloumn = sysCloumnService.selectSysCloumnById(sysCloumn.getId());
        if(sysCloumn.getPid()==null){
           sysCloumn.setPid(0L);
        }
        if (cloumn.getPid()==0&&sysCloumn.getPid()!=0){
            return AjaxResult.error("一级栏目不可选择父级栏目");
        }
        return toAjax(sysCloumnService.updateSysCloumn(sysCloumn));
    }

    /**
     * 删除【请填写功能名称】
     */
    @RequiresPermissions("system:cloumn:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sysCloumnService.deleteSysCloumnByIds(ids));
    }






    /**
     * 选择部门树
     */
    @GetMapping("/selectTree/{id}")
    public String selectDeptTree(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("cloumn",sysCloumnService.selectSysCloumnById(id));
        return prefix+ "/tree";
    }

    /**
     * 加载文章树
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = sysCloumnService.selectTree();
        return ztrees;
    }


    /**
     * 加载文章树
     */
    @GetMapping("/cloumnTreeData")
    @ResponseBody
    public List<Ztree> cloumnTreeData()
    {
        List<Ztree> ztrees = sysCloumnService.selectCloumnTree();
        return ztrees;
    }

    @GetMapping("/editid/{id}")
    public String editid(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("cloumn", sysCloumnService.selectSysCloumnById(id));
        return "/system/notice/edit";
    }

    /**
     * 修改栏目状态
     */
    @RequiresPermissions("system:cloumn:edit")
    @Log(title = "修改栏目状态", businessType = BusinessType.UPDATE)
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(SysCloumn sysCloumn)
    {
        return toAjax(sysCloumnService.updateSysCloumn(sysCloumn));
    }
}
